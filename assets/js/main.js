$(document).ready(function() {
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });

    //import header, footer
    $("#headerload").load("/includes/header.html");
    $("#footerload").load("/includes/footer.html");

    var scrollTop = '<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>';
    $("#headerload").after(scrollTop);

    var windowsize = $(window).width();
    if (windowsize < 767) {
        $(".box_search").css("display", "none");
        $('.list_news840').find('.thumb375x235').removeClass('thumb375x235').addClass('col-sm-6').addClass('col-xs-6').addClass('news');
        $('.list_news840').find('.desc_news').css('display', 'none');
        $('.list_news840').find('img').addClass('img-mobile');
        $('.list_news840').find('.desc_840').removeClass('desc_840').addClass('col-sm-6').addClass('col-xs-6');
        $('.list_news840').find('.f18').removeClass('f18');
        $('.no-padding').removeClass('no-padding');
        $('.title_detail').removeClass('title_detail').addClass('detail-title');
    } else {
        $('ul.nav li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
        });
    }

    //Check to see if the window is top if not then display button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 300) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    //Click event to scroll to top
    $('.scrollToTop').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('.list_slide_news_focus').owlCarousel({
        dots: false,
        margin: 17,
        autoplay: true,
        autoplayHoverPause: false,
        loop: true,
        items: 5,
        nav: true,
        navText: ["<i class='left_button'></i>", "<i class='right_button'></i>"],
    });


    $(".btn_show_hot").click(function() {
        $(".trend_news_cate").addClass('active');
        $(".trend_news_cate").removeClass('not_scroll');
    });
    $(".icon_close_trend").click(function() {
        $(".trend_news_cate").removeClass('active');
        $(".trend_news_cate").addClass('not_scroll');
    });
    $(".btn_search").click(function() {
        $("body").addClass('active');
        $(".shadow_search").addClass('active');
        $(".box_seach_show").addClass('active');
    });
    $("#close_search").click(function() {
        $("body").removeClass('active');
        $(".shadow_search").removeClass('active');
        $(".box_seach_show").removeClass('active');
    });
    $(".shadow_search").click(function() {
        $("body").removeClass('active');
        $(".shadow_search").removeClass('active');
        $(".box_seach_show").removeClass('active');
    });


    var theToggle = document.getElementById('toggle');

    // based on Todd Motto functions
    // https://toddmotto.com/labs/reusable-js/

    // hasClass
    function hasClass(elem, className) {
        return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
    }
    // addClass
    function addClass(elem, className) {
        if (!hasClass(elem, className)) {
            elem.className += ' ' + className;
        }
    }
    // removeClass
    function removeClass(elem, className) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(' ' + className + ' ') >= 0) {
                newClass = newClass.replace(' ' + className + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
    }
    // toggleClass
    function toggleClass(elem, className) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, " ") + ' ';
        if (hasClass(elem, className)) {
            while (newClass.indexOf(" " + className + " ") >= 0) {
                newClass = newClass.replace(" " + className + " ", " ");
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        } else {
            elem.className += ' ' + className;
        }
    }

    // theToggle.onclick = function() {
    //    toggleClass(this, 'on');
    //    return false;
    // }	
    // });

    $(function() {
        $('.scroll_news_title').slimScroll({
            height: '505px',
            allowPageScroll: true,
        });
    });
    $(function() {
        $('.h330').slimScroll({
            height: '330',
            railVisible: true,
            allowPageScroll: true,
            railColor: '#999',
        });
    });


    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 100 && !$('#not_scroll').hasClass('not_scroll')) {
            $(".trend_news_cate").addClass("active");
        } else if (scroll >= 100 && $('#not_scroll').hasClass('not_scroll')) {
            $(".trend_news_cate").removeClass("active");
            $(".trend_news_cate").addClass("not_scroll");
        } else if (scroll <= 100 || $('#not_scroll').hasClass('not_scroll') || $('.trend_news_cate').hasClass('active')) {
            $(".trend_news_cate").removeClass("active");
            $(".trend_news_cate").removeClass("not_scroll");
        }


    });


    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        var bar_top = $('.top_bar').height();
        var top_banner = $('.top_banner_site').height();
        var menu_main = $('.header_main').height();
        if (scroll >= bar_top + top_banner + menu_main) {
            $(".header_main ").addClass("active");
        } else {
            $(".header_main ").removeClass("active");
        }


    });

    $(function($) {
        $(window).on("load", function() {
            if ($(".mCustomScrollbar_Me").length) {
                $(".mCustomScrollbar_Me").mCustomScrollbar({
                    autoDraggerLength: true,
                    autoHideScrollbar: true,
                });
            }
        });
    });
})